srcdir ?= .
outdir ?= .
topsrcdir ?= $(srcdir)
topoutdir ?= $(outdir)

all: $(outdir)/horns-maroon-path.svg
all: $(outdir)/horns-gold-path.svg
all: $(outdir)/horns-black-path.svg

maroon = 8a181a
gold   = e5b217

$(outdir)/horns-%-text.svg: $(srcdir)/horns-black-text.svg
	mkdir -p -- $(@D)
	sed 's|#000000@|#$($*)|g' < $< > $@

$(topoutdir)/%-path.svg: $(topsrcdir)/%-text.svg
	mkdir -p -- $(@D)
	inkscape --file=$< --export-text-to-path --export-plain-svg=$@
$(topoutdir)/%-path.svg: $(topoutdir)/%-text.svg
	inkscape --file=$< --export-text-to-path --export-plain-svg=$@

clean:
	rm -f -- $(addprefix $(outdir)/,*-path.svg $(filter-out horns-black-text.svg,$(wildcard horns-*-text.svg)))

.PHONY: all clean
.SECONDARY:
.DELETE_ON_ERROR:
