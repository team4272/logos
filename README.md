This repository includes high-quality versions of the team logos.

The primary logo is the brass-bull.  Unfortunately, the best quality
version (`brass-bull.png`) that anyone can seem to find has quite a
bit of JPEG. `brass-bull2.xcf` is Luke Shumaker's attempt to clean it
up.

I have been unable to track down the low-color version of the brass
bull used on the team shirts.

The secondary logo is the horns.  There are 3 acceptable colors it can
be rendered in; maroon, gold, and black.  This repository only include
it in black.  It also requires that you have the nescessary fonts
installed.  However, running the `make` command will generate versions
in maroon and gold, and versions that don't require the fonts to be
installed (though the fonts must be installed to run the command).

The fonts needed are of the the team's primary and tertiary typefaces
(though the secondary font is not needed):
 - Primary: "Orator Std": the regular/medium variant of the Orator typeface from the
   Linotype foundry, in the OpenType Standard format
 - Secondary: "Gill Sans MT": the regular/medium/roman variant of the
   Gill Sans typeface from the MT foundry.  In version 2.00 of the
   font file, the " MT" suffix was dropped from the font name.
 - Tertiary: "Eras Bold ITC": the Bold variant of the Eras typeface
   from the ITC foundry

These should be on the high school's computers that have Adobe
Photoshop, as Gill Sans and Eras come with most versions of Microsoft
Office, and Orator comes with Adobe CS3 and above.
